zint (2.15.0-1) unstable; urgency=medium

  * New upstream release.
    + Install library files under multiarch location (Closes: #1060878).
  * Set libzint2.15 package as "Multi-Arch: same".

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 28 Feb 2025 22:50:03 +1100

zint (2.13.0-1) unstable; urgency=medium

  * New upstream release.
  * rules: no longer append "-Wl,--as-needed".
  * Build upstream man page from source.
    + Build-Depends += "pandoc".
  * Install upstream man page and documentation (with doc-base).
  * Updated Homepage to "https://zint.org.uk/" (Closes: #1021692).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 31 Dec 2023 20:19:44 +1100

zint (2.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.6.2.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 07 Feb 2023 02:14:01 +1100

zint (2.11.1-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depends += "libqt5svg5-dev".

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 19 Sep 2022 16:05:03 +1000

zint (2.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.6.1.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 05 Jun 2022 12:31:40 +1000

zint (2.10.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.6.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 01 Sep 2021 05:09:49 +1000

zint (2.9.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix stack-based buffer overflow in ean_leading_zeroes (see CVE-2021-
    27799) (Closes: #983610)

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 11 Mar 2021 12:37:31 -0600

zint (2.9.1-1) unstable; urgency=medium

  * New upstream release (Closes: #975161)
  * Bump Standards-Version
  * Mark libzint-dev Multi-Arch: same

 -- Jakob Haufe <sur5r@debian.org>  Fri, 04 Dec 2020 18:14:26 +0000

zint (2.9.0-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 17 Oct 2020 09:26:26 +1100

zint (2.8.0-1) unstable; urgency=low

  * Initial release (Closes: #732141).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 17 May 2020 23:28:08 +1000
